import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrimerPriceComponent } from './primer-price.component';

describe('PrimerPriceComponent', () => {
  let component: PrimerPriceComponent;
  let fixture: ComponentFixture<PrimerPriceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrimerPriceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PrimerPriceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
