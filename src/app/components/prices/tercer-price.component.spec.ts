import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TercerPriceComponent } from './tercer-price.component';

describe('TercerPriceComponent', () => {
  let component: TercerPriceComponent;
  let fixture: ComponentFixture<TercerPriceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TercerPriceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TercerPriceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
